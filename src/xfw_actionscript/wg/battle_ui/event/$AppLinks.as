package
{

internal class $AppLinks
{

/**
 *  @private
 *  This class is used to link additional classes into wg_*.swc
 *  beyond those that are found by dependecy analysis starting
 *  from the classes specified in manifest.xml.
 */

/**
 * UIs
 */

// minimapEntriesLibrary.swf
ArcadeCameraEntry;
CellFlashEntry;
DeadPointEntry;
StrategicCameraEntry;
VideoCameraEntry;
ViewPointEntry;
ViewRangeCirclesEntry;
VehicleEntry;

// sixthSense.swf
sixthSenseUI;

}

}
